<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class NamespaceSymfony
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InterfaceSymfony", mappedBy="namespaceSymfony")
     */
    private $interfacesSymfony;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClassSymfony", mappedBy="namespaceSymfony")
     */
    private $classesSymfony;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NamespaceSymfony", inversedBy="children")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\NamespaceSymfony", mappedBy="parent")
     */
    private $children;

    /**
     * NamespaceSymfony constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->interfacesSymfony = new ArrayCollection();
        $this->classesSymfony = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|InterfaceSymfony[]
     */
    public function getInterfacesSymfony()
    {
        return $this->interfacesSymfony;
    }

    public function addInterfacesSymfony($interfacesSymfony)
    {
        if (!$this->interfacesSymfony->contains($interfacesSymfony)) {
            $this->interfacesSymfony[] = $interfacesSymfony;
            $interfacesSymfony->setNamespaceSymfony($this);
        }

        return $this;
    }

    public function removeInterfacesSymfony($interfacesSymfony)
    {
        if ($this->interfacesSymfony->contains($interfacesSymfony)) {
            $this->interfacesSymfony->removeElement($interfacesSymfony);
            // set the owning side to null (unless already changed)
            if ($interfacesSymfony->getNamespaceSymfony() === $this) {
                $interfacesSymfony->setNamespaceSymfony(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClassSymfony[]
     */
    public function getClassesSymfony()
    {
        return $this->classesSymfony;
    }

    public function addClassesSymfony($classesSymfony)
    {
        if (!$this->classesSymfony->contains($classesSymfony)) {
            $this->classesSymfony[] = $classesSymfony;
            $classesSymfony->setNamespaceSymfony($this);
        }

        return $this;
    }

    public function removeClassesSymfony($classesSymfony)
    {
        if ($this->classesSymfony->contains($classesSymfony)) {
            $this->classesSymfony->removeElement($classesSymfony);
            // set the owning side to null (unless already changed)
            if ($classesSymfony->getNamespaceSymfony() === $this) {
                $classesSymfony->setNamespaceSymfony(null);
            }
        }

        return $this;
    }

    /**
     * NamespaceSymfony constructor.
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|NamespaceSymfony[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function addChild($child)
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild($child)
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }
}
