<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\ClassSymfony;
use App\Entity\InterfaceSymfony;
use App\Entity\NamespaceSymfony;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class MyNameOfParsingCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ParseSymfonyCommand constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:parsesymfony')
            ->setDescription('Parsing site api.symfony.com')
            ->setHelp('This command parses the site ...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $namespaceSymfony = new NamespaceSymfony();
        $namespaceSymfony->setUrl('http://api.andreybolonin.com/Symfony.html');
        $namespaceSymfony->setName('Symfony.html');
        $this->em->persist($namespaceSymfony);
        $this->em->flush();
        $this->getContent($namespaceSymfony->getUrl(), $namespaceSymfony);

        return 1;
    }

    //ne rugaysya
    public function getContent($url, $parent, $test = false)
    {
        $html = file_get_contents($url);
        $crawlerInterface = new Crawler($html);
        $interfaces = $crawlerInterface->filter('div.container-fluid.underlined > div.row > div.col-md-6 > em > a > abbr');
        //var_dump($interfaces);
        //  die;
        foreach ($interfaces as $interface) {
            $interfaceSymfony = new InterfaceSymfony();
            $interfaceSymfony->setName($interface->textContent);
            $str = $interface->getAttribute('title');
            $interfaceSymfony->setUrl('http://api.andreybolonin.com/'.str_replace('\\', '/', $interface->getAttribute('title').'.html'));
            $interfaceSymfony->setNamespaceSymfony($parent);
            $this->em->persist($interfaceSymfony);
            $this->em->flush();
        }
        $crawlerClass = new Crawler($html);
        $classes = $crawlerClass->filter('div.container-fluid.underlined > div.row > div.col-md-6 > a');
        foreach ($classes as $class) {
            // var_dump($parent);
            $classSymfony = new ClassSymfony();
            $classSymfony->setName($class->textContent);
            $classSymfony->setUrl('http://api.andreybolonin.com/'.$class->getAttribute('href'));
            $classSymfony->setNamespaceSymfony($parent);
            $this->em->persist($classSymfony);
        }
        $this->em->flush();
        $crawlerNamespace = new Crawler($html);
//        $crawler->addHtmlContent($html, 'UTF-8');
        $filtered = $crawlerNamespace->filter(' div#page-content > div.namespace-list > a');
        //var_dump($filtered->count());
        foreach ($filtered as $value) {
            $url = 'http://api.andreybolonin.com/'.str_replace('../', '', $value->getAttribute('href'));
            $namespaceSymfony = new NamespaceSymfony();
            $namespaceSymfony->setName($value->textContent);
            $namespaceSymfony->setUrl($url);
            $namespaceSymfony->setParent($parent);
            $this->em->persist($namespaceSymfony);
            $this->em->flush();
            //var_dump($namespaceSymfony->getId());
            $this->getContent($url, $namespaceSymfony);
        }
    }
}
