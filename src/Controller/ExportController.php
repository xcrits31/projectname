<?php

namespace App\Controller;

use App\Entity\User;
use Endroid\QrCode\QrCode;
use Knp\Snappy\Pdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExportController.
 */
class ExportController extends AbstractController
{
    /**
     * @Route("/xls", name="xls_hello_world")
     */
    public function xlsHelloWorld()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__.'/../../public/xls/hello_world.xlsx');

        $response = new BinaryFileResponse(__DIR__.'/../../public/xls/hello_world.xlsx');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'hello_world.xlsx');

        return $response;
    }

    /**
     * @Route("/pdf", name="pdf_hello_world")
     */
    public function pdfGeneration(Pdf $generator)
    {
        $generator->generate('http://www.google.com', __DIR__.'/../../public/pdf/hello_world.pdf');

        $response = new BinaryFileResponse(__DIR__.'/../../public/pdf/hello_world.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'hello_world.pdf');

        return $response;
    }

    /**
     * @Route("/qr", name="qr_code").
     */
    public function genratorQRCode()
    {
        $qrCode = new QrCode('https://andreybolonin.com/phpschool/');
        $qrCode->writeFile(__DIR__.'/../../public/qr/qrcode.png');

        $response = new BinaryFileResponse(__DIR__.'/../../public/qr/qrcode.png');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'qrcode.png');

        return $response;
    }

    /**
     * @Route("/download/user/xls", name="download_user_xls")
     */
    public function downloadUsers()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();

        $excel = new Spreadsheet();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();

        $sheet->setTitle('Пользователи');

        $row = 3;
        $style = [
            'borders' => [
                'allborders' => [
                    'style' => Border::BORDER_THIN,
                ],
            ],
        ];

        /** @var User $user */
        foreach ($users as $user) {
            $sheet
                ->setCellValue('A1', 'Имя')
                ->setCellValue('B1', 'ID')
                ->setCellValue('A'.$row, $user->getUsername())
                ->setCellValue('B'.$row, $user->getId());
            ++$row;
        }
        --$row;
        $sheet->getStyle('B2:P'.$row)->applyFromArray($style);
        $objWriter = new \PhpOffice\PhpSpreadsheet\Writer\Xls($excel);
        $filename = __DIR__.'/../../public/xls/users.xls';
        $objWriter->save($filename);

        return new BinaryFileResponse($filename);
    }
}
