<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class MailerController extends AbstractController
{
    /**
     * @Route("/email")
     */
    public function sendEmail(MailerInterface $mailer)
    {
        $email = (new Email())
            ->from('XCrits31@gmail.com')
            ->to('XCrits31@gmail.com')
            ->subject('ne rugaysya')
            ->text('ok? pls write it!!!!!');

        $mailer->send($email);

        return new JsonResponse();
    }
}
