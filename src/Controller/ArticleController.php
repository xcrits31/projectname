<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/article/{_locale}", name="article_index", requirements={"_locale"="en|ru"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Article::class)->findAll();

        return $this->render('article/index.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/article/{_locale}/create", name="article_create", requirements={"_locale"="en|ru"})
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/create.html.twig', ['article' => $article, 'form' => $form->createView()]);
    }

    /**
     * @Route("/article/{_locale}/show/{id}", name="article_show", requirements={"_locale"="en|ru"})
     */
    public function showAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        return $this->render('user/show.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/article/{_locale}/edit/{id}", name="article_edit", requirements={"_locale"="en|ru"})
     */
    public function editAction(int $id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID '.$id.' not found!');
        }

        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_show', ['id' => $id]);
        }

        return $this->render('article/edit.html.twig', [
            'article' => $article,
            'editForm' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/article/{_locale}/delete/{id}", name="article_delete", requirements={"_locale"="en|ru"})
     */
    public function deleteAction(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID'.$id.'not found!');
        }

        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('article_index');
    }

    /**
     * @Route("/article/locale/{locale}", name="article_locale")
     */
    public function changeLocaleAction(Request $request, string $locale)
    {
        $request->getSession()->set('_locale', $locale);

        return $this->redirectToRoute('article_index', ['_locale' => $locale]);
    }
}
