<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class SecurityControllerTest
 *
 * @package App\Tests\Controller
 */
class SecurityControllerTest extends WebTestCase
{
    public function testRegisterAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/register');
        $button = $crawler->selectButton('user_create[submit]');
        $form = $button->form([
            'user_create[username]' => 'admin',
            'user_create[password][first]' => 'admin',
            'user_create[password][second]' => 'admin',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testLoginAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'username',
            'password' => 'password'
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
