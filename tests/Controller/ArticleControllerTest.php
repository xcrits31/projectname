<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ArticleControllerTest.
 */
class ArticleControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'username',
            'password' => 'password',
        ]);
        $client->submit($form);
        $client->request('GET', '/article/en');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'username',
            'password' => 'password',
        ]);

        $client->submit($form);
        $crawler = $client->request('GET', '/article/en/create');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'the first one',
            'article[description]' => 'justCreatedDescription',
            'article[created_at][date][month]' => '4',
            'article[created_at][date][day]' => '21',
            'article[created_at][date][year]' => '2020',
            'article[created_at][time][hour]' => '12',
            'article[created_at][time][minute]' => '4',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testEditAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'username',
            'password' => 'password',
        ]);
        $client->submit($form);
        $crawler = $client->request('GET', '/article/en/edit/1');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'edited mess',
            'article[description]' => ' description was edited',
            'article[created_at][date][month]' => '8',
            'article[created_at][date][day]' => '23',
            'article[created_at][date][year]' => '2019',
            'article[created_at][time][hour]' => '18',
            'article[created_at][time][minute]' => '4',
        ]);

        $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteArticle()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'username',
            'password' => 'password',
        ]);
        $client->submit($form);
        $client->request('GET', '/article/en/delete/1');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
    public function testSendEmail()
    {
        $client = static::createClient();
        $client->request('GET', '/email');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEmailCount(1);

    }
}

